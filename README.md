# Go cron runner

Creates a cross platform cron job runner in Go.

## Usage

| parameter | default    | notes |
| --------- | ---------- | ----- |
| port      | 8000       | Port to launch a web server with a JSON monitoring interface |
| config    | config.yml | Configuration file to use |

## Build

*see [Gitlab CI file](.gitlab-ci.yml)*

## Dependencies

This library makes use of:

 * https://github.com/bamzi/jobrunner
 * https://github.com/go-yaml/yaml
