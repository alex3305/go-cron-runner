package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/bamzi/jobrunner"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
)

var (
	port       *int
	configFile *string
	config     []CronJob
)

type CronJob struct {
	Cron string `yaml:"cron"`
	Exec string `yaml:"exec"`
	Args string `yaml:"args,omitempty"`
}

func main() {
	readParams()
	readSettings()
	jobrunner.Start()
	readJobs()

	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		json.NewEncoder(w).Encode(jobrunner.StatusJson())
	})
	http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)
}

func (c CronJob) Run() {
	var err error
	if c.Args == "" {
		err = exec.Command(c.Exec).Run()
	} else {
		err = exec.Command(c.Exec, c.Args).Run()
	}

	if err != nil {
		log.Println(err)
	}
}

func readJobs() {
	for _, v := range config {
		jobrunner.Schedule(v.Cron, v)
	}
}

func readParams() {
	port = flag.Int("port", 8000, "Http port for monitoring")
	configFile = flag.String("config", "config.yml", "Config file")
	flag.Parse()
}

func readSettings() {
	file, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalln("Could not read config file ", *configFile)
	}

	err = yaml.Unmarshal(file, &config)
	if err != nil {
		log.Fatalln("Could not parse config file ", *configFile)
	}
}
